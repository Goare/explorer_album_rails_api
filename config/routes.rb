Rails.application.routes.draw do
  post 'qiniu_callback' => 'albums#qiniu_callback'
  post 'send_sms' => 'accounts#send_sms'


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :albums do
    resources :photos, only: [:index]
  end

  resources :accounts, only: [:create]
  resources :sessions, only: [:create, :destroy]

end

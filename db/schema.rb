# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170601183918) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "albums", force: :cascade do |t|
    t.string   "name"
    t.string   "up_token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "album_hash"
    t.integer  "user_id"
    t.index ["album_hash"], name: "index_albums_on_album_hash", unique: true, using: :btree
    t.index ["user_id"], name: "index_albums_on_user_id", using: :btree
  end

  create_table "photos", force: :cascade do |t|
    t.integer  "album_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "photo_hash"
    t.integer  "width"
    t.integer  "height"
    t.string   "format"
    t.index ["album_id"], name: "index_photos_on_album_id", using: :btree
    t.index ["photo_hash"], name: "index_photos_on_photo_hash", using: :btree
  end

  create_table "sms", force: :cascade do |t|
    t.string   "mobile"
    t.string   "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "password_digest"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "token"
    t.string   "mobile"
  end

  add_foreign_key "photos", "albums"
end

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


Sm.create!(mobile: 18621631350, code: 3305)


# Photo.create!(photo_hash: 'Fmj3z4fH2TyH_jqbnusXLHz-mKET', album_id: 1)
# Photo.create!(photo_hash: 'FiFQBPZt6lHg_1a_ku1IJkcUYao1', album_id: 1)
# Photo.create!(photo_hash: 'FnPXmom97a2MvOhSq6tlicq8AZJ3', album_id: 1)
# Photo.create!(photo_hash: 'FpsnaFZU6txIGY1i1_y__jn03JQN', album_id: 1)
# Photo.create!(photo_hash: 'FkokeF9MZJqfZweQ5j2u_wQWJmpx', album_id: 1)
# Photo.create!(photo_hash: 'FppGPw2OC7v5kEDX90Tuz7QalzKJ', album_id: 1)
# Photo.create!(photo_hash: 'Fhtx4HYu7-wJIygQ6UGD3lW2y4f2', album_id: 1)
# Photo.create!(photo_hash: 'FukB3nYSaU2aoeGs6i2y0s80n44J', album_id: 1)
# Photo.create!(photo_hash: 'Fl3MZEakHf9YLNdSUu2XNle-N7e-', album_id: 1)
# Photo.create!(photo_hash: 'Fqr1g77iWykDCd5QwvCg5-cSpsZG', album_id: 1)
# Photo.create!(photo_hash: 'FrbEenPagmwAB7yk1Y2EFOeo6t6M', album_id: 1)

# Photo.create!(photo_hash: 'Fmj3z4fH2TyH_jqbnusXLHz-mKET', album_id: 1, width: 555, height: 720, format: 'jpeg')
# Photo.create!(photo_hash: 'FpsnaFZU6txIGY1i1_y__jn03JQN', album_id: 1, width: 1594, height: 883, format: 'png')
# Photo.create!(photo_hash: 'Fq82y2qBCjzb3_4rd-0jVe5F-A8t', album_id: 1, width: 1030, height: 563, format: 'png')
# Photo.create!(photo_hash: 'Fsg5ELK4etM9HPXVGUUjWUQlK6QV', album_id: 1, width: 670, height: 419, format: 'png')
# Photo.create!(photo_hash: 'FkokeF9MZJqfZweQ5j2u_wQWJmpx', album_id: 1, width: 360, height: 968, format: 'jpeg')

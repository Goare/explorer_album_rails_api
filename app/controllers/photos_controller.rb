class PhotosController < ApplicationController
  before_action :get_album, only: [:index]

  def index
    @photos = @album.photos
    render json: {status: 200, data: @photos}
  end

  private
    def get_album
      @album = Album.find_by!(album_hash: params[:album_id])
    end
end

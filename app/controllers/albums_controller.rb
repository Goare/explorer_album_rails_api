class AlbumsController < ApplicationController
  before_action :authenticate, only: [:index, :create]
  before_action :get_album, only: [:show, :update, :destroy]

  def index
    @albums = @current_user.albums
    render json: {status: 200, data: @albums}
  end

  def create
    @album = Album.new(params.permit(:name))
    @album.album_hash = generate_album_hash
    @album.up_token = generate_qiniu_uptoken
    @album.user = @current_user

    @album.save!
    render json: {status: 200, data: @album}
  end

  def show
    render json: {status: 200, data: @album}
  end

  def update
    @album.update(params.permit(:name))
    head :no_content
  end

  def destroy
    @album.destroy
    head :no_content
  end

  def qiniu_callback
    photo_params = params.permit(:photo_hash, :album_id, :width, :height, :format);
    Photo.create!(photo_params);

    render json: {status: 200, data: photo_params}

    <<-DOC
    sample res:
    {
      status: 200,
      data: {
        "album_id": "4",
        "photo_hash": "Fmj3z4fH2TyH_jqbnusXLHz-mKET"
      }
    }

    Photo.create!(photo_hash: photo_hash, album_id: album_id)
    DOC
  end

  private

    def get_album
      @album = Album.find_by!(album_hash: params[:id])
    end

    def generate_qiniu_uptoken
      bucket = 'default'
      key = nil

      put_policy = Qiniu::Auth::PutPolicy.new(
        bucket,
        key,
        3600 * 24 * 7
      )

      callback_url = ENV['qiniu_callback_url']
      callback_body = 'photo_hash=$(key)&width=$(imageInfo.width)&height=$(imageInfo.height)&format=$(imageInfo.format)&album_id=$(x:album_id)'

      put_policy.callback_url = callback_url
      put_policy.callback_body = callback_body

      uptoken = Qiniu::Auth.generate_uptoken(put_policy)
    end

    def generate_album_hash
      require 'securerandom'
      SecureRandom.uuid
    end
end

class SessionsController < ApplicationController
  def create
    name = params.permit(:name)[:name]
    password = params.permit(:password)[:password]

    @user = User.find_by(name: name)

    if @user and @user.authenticate(password)
      render json: {status: 200, data: {token: @user.token}}
    else
      render json: {status: 401, error: "user name or password was wrong"}
    end
  end

  def destroy
    # delete local user token
  end
end

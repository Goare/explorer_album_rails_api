class AccountsController < ApplicationController
  def create
    # name, password, mobile, sms_code
    # 验证sms_code, 创建用户
    mobile = params[:mobile]
    sms_code = params[:sms_code]

    @mobile = Sm.find_by(mobile: mobile)
    if @mobile and verify_sms_code(sms_code)
      @user = User.create!(params.permit(:name, :password, :mobile))
      @user.token = @user.token
      @user.save

      render json: {status: 200, data:  {token: @user.token}}
    else
      render json: {status: 200, error: '短信码验证失败'}
    end
  end

  def verify_captcha
    verify_rucaptcha?
  end

  # 验证v_code, 发送短信，插入sms_code
  def send_sms   # post {_rucaptcha, mobile}
    mobile = params[:mobile]
    if (verify_captcha)
      require 'uri'
      require 'net/http'

      post_uri = 'https://sms.yunpian.com/v2/sms/single_send.json'
      if @mobile = Sm.find_by(mobile: mobile)
        sms_code = @mobile.code
      else
        sms_code = generate_sms_code
        Sm.create!(mobile: mobile, code: sms_code)
      end
      tmpl = "【桤木相册】您的验证码是#{sms_code}。如非本人操作，请忽略本短信"

      uri = URI.parse(post_uri)
      res = Net::HTTP.post_form(uri, {
          apikey: '50eff5326d62725e5158338a6cdd50d0',
          mobile: mobile,
          text: tmpl
      })

      render json: {status: 200, data: 'ok'}
    else
      render json: {status: 200, error: 'captacha wrong'}
    end
  end

  def verify_sms_code(code)
    @mobile.code.eql?(code)
  end

  private
    # rucaptcha hack below
    def rucaptcha_sesion_key_key
      session_id = session.respond_to?(:id) ? session.id : session[:session_id]
      ['rucaptcha-session', session_id].join(':')
    end

    def verify_rucaptcha?(resource = nil, opts = {})
      opts ||= {}

      store_info = RuCaptcha.cache.read(rucaptcha_sesion_key_key)
      # make sure move used key
      RuCaptcha.cache.delete(rucaptcha_sesion_key_key) unless opts[:keep_session]

      # Make sure session exist
      if store_info.blank?
        return false
      end

      # Make sure not expire
      if (Time.now.to_i - store_info[:time]) > RuCaptcha.config.expires_in
        return false
      end

      # Make sure parama have captcha
      captcha = (params[:_rucaptcha] || '').downcase.strip
      if captcha.blank?
        return false
      end

      if captcha != store_info[:code]
        return false
      end

      true
    end
    # rucaptcha hack above

    def generate_sms_code
      code = ""
      4.times do
        code += Random.new.rand(0..9).to_s
      end

      code
    end
end

class Album < ApplicationRecord
  # validates :name, :hash, :up_token, presence: true

  has_many :photos
  belongs_to :user

  validates :name, presence: true, uniqueness: true
end

class User < ApplicationRecord
  has_secure_password

  validates :name, presence: true, uniqueness: true
  validates :password, presence: true

  has_many :albums

  def token
    Token.encode(user_id: self.id)
  end
end
